Set-location -Path 'D:\opensmt\'
npm run build
Set-location -Path 'D:\opensmt-backend\'
pkg -t node18-win-x64 .\src\opensmt.js
Copy-Item -Path 'D:\opensmt-backend\opensmt.exe' -Destination 'D:\opensmt-build\dist\smt.exe'
Remove-Item 'D:\opensmt-build\public\' -Recurse
Copy-Item -Path 'D:\opensmt\dist\' -Destination 'D:\opensmt-build\public' -Recurse
Set-location -Path 'D:\opensmt-build\'
npm run start