# Opensmt-GUI-Build

用于将 Opensmt-GUI 项目打包至各个平台的项目。


## 使用方法

1. 将 opensmt-GUI 项目下载至本地。
2. 在 opensmt-GUI 目录下运行以下命令：

```sh
npm install && npm run build
```
3. 将 opensmt-GUI 目录下 `/dist` 文件夹复制到本文件夹，并重命名为 `/public`。
4. 根据系统环境安装 `electron-builder` 版本并执行 `electron-builder ./dist/main.js`


## 版权声明

Copyright ©2024 opensmt-dev-team, All Rights Reserved.