// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
import { OpenDialogOptions } from 'electron';
import { contextBridge, ipcRenderer } from 'electron/renderer'

window.addEventListener("DOMContentLoaded", () => {
    const replaceText = (selector: string, text: string) => {
        const element = document.getElementById(selector);
        if (element) {
            element.innerText = text;
        }
    };

    for (const type of ["chrome", "node", "electron"]) {
        replaceText(`${type}-version`, process.versions[type as keyof NodeJS.ProcessVersions]);
    }
});

// 处理后端接口
contextBridge.exposeInMainWorld('electronAPI', {
    createDataBase: (password: string) => ipcRenderer.invoke('CLI:createDataBase', password),
    connectDataBase: (database: string, password: string) => ipcRenderer.invoke('CLI:connectDataBase', database, password),
    createKeyPair: (name: string, email: string, upload: boolean) => ipcRenderer.invoke('CLI:createKeyPair', name, email, upload),
    importPublicKey: () => ipcRenderer.invoke('CLI:importPublicKey'),
    exportPublicKey: (id:string) => ipcRenderer.invoke('CLI:exportPublicKey',id),
    uploadPublicKey: (id:string) => ipcRenderer.invoke('CLI:uploadPublicKey',id),
    sendMessage: () => ipcRenderer.invoke('CLI:sendMessage'),
    getMessage: () => ipcRenderer.invoke('CLI:getMessage'),
    chooseFiles: (conf: OpenDialogOptions) => ipcRenderer.invoke('Files:chooseFiles', conf),
    listKeys: () => ipcRenderer.invoke('CLI:listKeys')
})
