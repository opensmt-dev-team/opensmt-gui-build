import { app, BrowserWindow, dialog, ipcMain, OpenDialogOptions } from "electron";
import * as path from "path";
import * as CLI from "./CLI";
function createWindow() {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
        },
        width: 800,
    });
    mainWindow.maximize();
    // and load the index.html of the app.
    mainWindow.loadFile(path.join(__dirname, "../public/index.html"));

    // Open the DevTools.
    // mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
    ipcMain.handle(`CLI:createDataBase`, (e, password: string) => CLI.createDataBase(password))
    ipcMain.handle(`CLI:connectDataBase`, (e, databasePos: string, password: string) => CLI.connectDataBase(databasePos, password))
    ipcMain.handle(`CLI:createKeyPair`, (e, name: string, email: string, upload: boolean) => CLI.createKeyPair(name, email, upload))
    ipcMain.handle(`CLI:exportPublicKey`, (e, id: string) => CLI.exportPublicKey(id))
    ipcMain.handle(`CLI:uploadPublicKey`, (e, id: string) => CLI.uploadPublicKey(id))
    ipcMain.handle(`Files:chooseFiles`, (e, conf: OpenDialogOptions) => dialog.showOpenDialog(conf))
    ipcMain.handle(`CLI:listKeys`, () => CLI.listKeys())
    createWindow();
    app.on("activate", function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
