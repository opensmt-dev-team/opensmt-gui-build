import { execFile } from 'child_process';
import { randomUUID } from 'crypto';
import { dialog } from 'electron'
import * as fsExtra from 'fs-extra'
import os from 'os'
import path from 'path';
import axios from 'axios'
const serverIp = `http://192.168.1.6:10086`
const execFileName = path.join(__dirname, os.type() == `Linux` ? `smt` : `smt.exe`)
interface outParams {
    code: number,
    msg: string,
    err: string,
    data?: {
        [key: string]: any;
    }
}
let lastdatabasePos = ``, lastdatabasePassword = ``;
/**
 * 创建数据库
 * @param {string} password 数据库初始化密码
 * @returns {Promise<void>}
 */
function createDataBase(password: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        dialog.showSaveDialog({ title: `选择保存数据库位置`, filters: [{ name: `数据库文件`, extensions: [`db`] }] }).then((res) => {
            const databasePos = res.filePath
            if (fsExtra.existsSync(databasePos)) {
                reject(`数据库已存在`);
                return;
            }
            const params = {
                password: password,
                database: databasePos
            }
            const paramsPath = path.join(__dirname, `./${randomUUID()}.json`)
            fsExtra.writeFileSync(paramsPath, JSON.stringify(params), "utf-8")
            execFile(execFileName, [`--cli`, `--params=${paramsPath}`], {
                cwd: __dirname,
                encoding: `base64`
            }, (err, stdout) => {

                stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
                stdout = stdout.replace(`secp256k1 unavailable, reverting to browser version`, ``)
                stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);

                fsExtra.remove(paramsPath);
                if (err) {
                    console.error(err);
                    return;
                }
                const data: outParams = JSON.parse(stdout);
                if (data.code === 0 || data.code === 2) {
                    resolve();
                }
                else {
                    reject(data.msg);
                }
            })

        })
    })
}
/**
 * 链接数据库
 * @param {string} databasePos 要链接的数据库的位置
 * @param {string} password 数据库密码
 * @returns {Promise<void>}
 */
function connectDataBase(databasePos: string, password: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        if (!fsExtra.existsSync(databasePos)) {
            reject(`数据库不存在`);
            return;
        }
        const params = {
            database: databasePos,
            password: password
        }

        const paramsPath = path.join(__dirname, `./${randomUUID()}.json`)
        fsExtra.writeFileSync(paramsPath, JSON.stringify(params), "utf-8")
        execFile(execFileName, [`--cli`, `--params=${paramsPath}`], {
            cwd: __dirname,
            encoding: `base64`
        }, (err, stdout) => {

            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
            stdout = stdout.replace(`secp256k1 unavailable, reverting to browser version`, ``)
            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);

            console.log(`------------`)
            console.log(stdout)
            console.log(`------------`)
            fsExtra.remove(paramsPath);
            const data: outParams = JSON.parse(stdout);
            if (data.code === 0) {
                lastdatabasePos = databasePos;
                lastdatabasePassword = password;
                resolve();
            }
            else {
                reject(data.msg);
            }
        })
    })
}
function uploadPublicKey(id:string):Promise<void>
{
    return new Promise<void>((resolve,reject)=>
    {
        exportPublicKey(id).then((res)=>
        {
            axios.post(`${serverIp}/uploadKey`,JSON.parse(res)).then(()=>
            {
                resolve();
            }).catch((err)=>
            {
                reject(err);
            })
        }).catch((err)=>
        {
            reject(err);
        })
    })
}
/**
 * 创建密匙对
 * @param {string} databasePos 对应的数据库
 * @param {string} password 数据库密码
 * @returns {Promise<string>}
 */
function createKeyPair(name: string, email: string, upload: boolean): Promise<string> {
    return new Promise((resolve, reject) => {
        if (lastdatabasePos === ``) {
            reject(`Database Not Connected`);
            return;
        }
        if (!fsExtra.existsSync(lastdatabasePos)) {
            reject(`数据库不存在`);
            return;
        }
        const params = {
            database: lastdatabasePos,
            password: lastdatabasePassword,
            generate: true,
            email: email,
            name: name
        }
        const paramsPath = path.join(__dirname, `./${randomUUID()}.json`)
        fsExtra.writeFileSync(paramsPath, JSON.stringify(params), "utf-8")
        execFile(execFileName, [`--cli`, `--params=${paramsPath}`], {
            cwd: __dirname,
            encoding: `base64`
        }, (err, stdout) => {

            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
            stdout = stdout.replace(`secp256k1 unavailable, reverting to browser version`, ``)
            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);


            const data: outParams = JSON.parse(stdout);
            if (data.code === 0) {
                if(upload)
                {
                    uploadPublicKey(data.data.id).then(()=>
                    {
                        resolve(data.data.id);
                    }).catch((err)=>
                    {
                        console.error(err);
                        reject(`密匙创建成功，但上传至服务器失败`);
                    })
                }
            }
            else {
                reject(data.err);
            }
        })
    })
}


/**
 * 导入公匙
 * @param {string} databasePos 对应的数据库
 * @param {string} password 数据库密码
 * @returns {Promise<string>}
 */
function importPublicKey(databasePos: string): Promise<string> {
    return new Promise((resolve, reject) => {
        if (!fsExtra.existsSync(databasePos)) {
            reject(`数据库不存在`);
            return;
        }
        dialog.showOpenDialog({ title: `请选择公匙文件` }).then(() => {
            execFile(execFileName, [`--cli`], {
                cwd: __dirname,
                encoding: `base64`
            }, (err, stdout) => {

                stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
                stdout = stdout.replace(`secp256k1 unavailable, reverting to browser version`, ``)
                stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
                const data: outParams = JSON.parse(stdout);
                if (data.code === 0) {
                    resolve(data.data.pubKeyId);
                }
                else {
                    reject(data.err);
                }
            })
        })
    })
}

/**
 * 导出私匙对应的公匙
 * @param {string} id
 * @returns {Promise<string>}
 */
function exportPublicKey(id: string): Promise<string> {
    return new Promise((resolve, reject) => {
        if (!fsExtra.existsSync(lastdatabasePos)) {
            reject(`数据库不存在`);
            return;
        }
        const params = {
            export: true,
            id: id,
            database: lastdatabasePos,
            password: lastdatabasePassword
        }
        const paramsPath = path.join(__dirname, `./${randomUUID()}.json`)
        fsExtra.writeFileSync(paramsPath, JSON.stringify(params), "utf-8")
        execFile(execFileName, [`--cli`, `--params=${paramsPath}`], {
            cwd: __dirname,
            encoding: `base64`
        }, (err, stdout) => {
            if (err) {
                console.error(err);
                reject(err);
                return;
            }
            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
            stdout = stdout.replace(`secp256k1 unavailable, reverting to browser version`, ``)
            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
            fsExtra.remove(paramsPath);
            console.log(stdout)
            resolve(stdout);
        })
    })
}

/**
 * 列出密匙
 * @returns {Promise<{keyPair:{id:string, name:string, email:string}[],publicKey:{id:string, name:string, email:string}[]}>}
 */
function listKeys(): Promise<{ keyPair: { id: string, name: string, email: string }[], publicKey: { id: string, name: string, email: string }[] }> {
    return new Promise((resolve, reject) => {
        if (lastdatabasePos === ``) {
            reject(`Database Not Connected`);
            return;
        }
        if (!fsExtra.existsSync(lastdatabasePos)) {
            reject(`数据库不存在`);
            return;
        }
        const params = {
            database: lastdatabasePos,
            password: lastdatabasePassword,
            list: true
        }
        const paramsPath = path.join(__dirname, `./${randomUUID()}.json`)
        fsExtra.writeFileSync(paramsPath, JSON.stringify(params), "utf-8")
        execFile(execFileName, [`--cli`, `--params=${paramsPath}`], {
            cwd: __dirname,
            encoding: `base64`
        }, (err, stdout) => {

            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);
            stdout = stdout.replace(`secp256k1 unavailable, reverting to browser version`, ``)
            stdout = Buffer.from(stdout, `base64`).toString(`utf-8`);

            const data: outParams = JSON.parse(stdout);
            if (data.code === 0) {

                resolve({ keyPair: data.data.keyPair, publicKey: data.data.publicKey });
            }
            else {
                console.error(err);
                reject(data.err);
            }
        })
    })
}

export { createDataBase, connectDataBase, createKeyPair, importPublicKey,uploadPublicKey, listKeys, exportPublicKey }